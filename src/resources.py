﻿################################################################################
# Filename: resource.py                                                        #
# Created by: Venceslas Duet                                                   #
# Created at: 05-04-2018                                                       #
# Last update at: 03-15-2022                                                   #
# Description: High level class for manage resource                            #
# Licence: GNU GPL v3.0 (See LICENSE.MD for more information)                  #
################################################################################

# TODO: Import metrics from configuration

import json

import pygame
from graphics import text, frame
from graphics.metrics import Size

import internal


class Resources:
    # Fonts Resources
    FONT_DEFAULT = 0

    font_names = ["DEFAULT"]
    fonts = []

    # Misc Image Resources
    MISC = 0

    MISC_ICON_32 = 0
    PIXEL_LOGO = 1

    misc_img_names = ["ICON", "PIXEL_LOGO"]
    misc_images = []

    # UI Image and color Resources
    UI = 1

    UI_TOOLBAR_BACKGROUND = 0
    UI_CLOCK_BACKGROUND = 1
    UI_CARD_BACKGROUND = 2
    UI_THUMBNAIL_BACKGROUND = 3

    ui_img_names = ["TOOLBAR_BACKGROUND", "CLOCK_BACKGROUND", "CARD_BACKGROUND", "THUMBNAIL_BACKGROUND"]
    ui_images = []

    # Icon Resources
    ICON = 2

    ICON_JOYSTICK = 0
    ICON_BUTTON_HOME = 1

    icon_img_names = ["JOYSTICK", "BUTTON_HOME"]
    icon_images = []

    # Metric resources
    METRIC_WINDOW_WIDTH = 0
    METRIC_WINDOW_HEIGHT = 1
    METRIC_TOOLBAR_HEIGHT = 2
    METRIC_CLOCK_MARGIN = 3
    METRIC_CARD_ICON_SIZE = 4
    METRIC_CARD_MARGIN = 5
    METRIC_CARD_PADDING = 6
    METRIC_BUTTON_ICON_MARGIN = 7

    metric_names = [
        "WINDOW_WIDTH",
        "WINDOW_HEIGHT",
        "TOOLBAR_HEIGHT",
        "CLOCK_MARGIN",
        "CARD_ICON_SIZE",
        "CARD_MARGIN",
        "CARD_PADDING",
        "BUTTON_ICON_MARGIN"
    ]
    metric = []

    # Color resources
    COLOR_BACKGROUND = 0

    color_names = [
        "background"
    ]
    colors = []

    _loaded = False
    _path = None

    use_smooth_resize = False

    @staticmethod
    def extractColors(names: list[str], desc_info: dict[str, list[int]]) -> list[list[int]]:
        length = len(names)
        ret = [[]] * length

        for i in range(length):
            if names[i] in desc_info:
                col = desc_info[names[i]]
                Resources.check_color(col)
                ret[i] = col
            else:
                raise ValueError("Color called {} is unavailable. Please check desc.json".format(names[i]))

        return ret

    @staticmethod
    def check_color(color : list[int]):
        length = 0
        for i in color:
            if 0 > i or i > 255:
                raise ValueError("color must be an array of integer with value between 0 and 255")
            length += 1
        if length < 3 or length > 4:
            raise ValueError("color must have 3 or 4 components (alpha is optional)")

    @staticmethod
    def extractImages(path: str, names: list[str], desc_info):
        length = len(names)
        ret = [None] * length

        for i in range(length):
            if names[i] in desc_info:
                ret[i] = Resources.generateImageElement(path, names[i], desc_info[names[i]])
            else:
                raise ValueError("Graphical element called {} for Misc module is unavailable. Please check desc.json".format(names[i]))

        return ret

    @staticmethod
    def extractMetrics(names, desc_info):
        length = len(names)
        ret = [None] * length

        for i in range(length):
            if names[i] in desc_info:
                tmp = desc_info[names[i]]
                try:
                    ret[i] = float(tmp)
                except:
                    raise ValueError("Value called {} have for value {} which is not an integer. Please check desc.json".format(names[i], tmp))
            else:
                raise ValueError("Metric called {} for Misc module is unavailable. Please check desc.json".format(names[i]))

        return ret

    @staticmethod
    def load(path):
        Resources._path = path
        images_desc, fonts_desc = Resources.readFiles(path)
        Resources._loaded = False
        # Read descriptor for images
        if (
                "misc" in images_desc and
                "ui" in images_desc and
                "icon" in images_desc and
                "color" in images_desc and
                "metric" in images_desc
        ):
            # Image Resources
            # Miscellaneous images
            Resources.misc_images = Resources.extractImages(path, Resources.misc_img_names, images_desc["misc"])

            # UI images
            Resources.ui_images = Resources.extractImages(path, Resources.ui_img_names, images_desc["ui"])

            # Menu images
            Resources.icon_images = Resources.extractImages(path, Resources.icon_img_names, images_desc["icon"])

            # Colors
            Resources.colors = Resources.extractColors(Resources.color_names, images_desc["color"])

            # Metrics
            Resources.metric = Resources.extractMetrics(Resources.metric_names, images_desc["metric"])
        else:
            raise ValueError("All modules (misc, ui and icon) are not allowed. Please check desc.json")

        try:
            Resources.use_smooth_resize = bool(images_desc["use_smooth_resize"])
        except:
            Resources.use_smooth_resize = False

        # Read descriptor for fonts
        Resources.fonts = []
        for i in Resources.font_names:
            if i in fonts_desc:
                Resources.fonts.append(Resources.generateFontElement(path, i, fonts_desc[i]))
            else:
                raise ValueError("Element called " + i + " for font is unavailable. Please check font.json")

        Resources._loaded = True

    @staticmethod
    def reload():
        Resources._loaded = False
        Resources.load(Resources._path)

    @staticmethod
    def generateFontElement(path, name, data):
        if "image" in data:
            try:
                image_path = path + "/" + data["image"]
                image = pygame.image.load(image_path)
            except:
                raise ValueError(
                    "For create element, 'image' need to be valid path for valid image in " + name + " element")
            if "canvas" in data:
                if internal.correct_tuple(data["canvas"], int, 2):
                    return text.Text(image, Size.from_tuple(data["canvas"]), data["ascender-height"], data["baseline"])
                else:
                    raise ValueError(
                        "For create Font element, 'canvas' need to be (int width, int height) in " + name + " element")
            else:
                raise ValueError(
                    "For create Font element, it need to have 'canvas' as property in " + name + " element")
        else:
            raise ValueError("For create element, it need to have 'image' as property in " + name + " element")

    @staticmethod
    def generateImageElement(path: str, name: str, data):
        if "type" in data:
            if "image" in data:
                try:
                    image_path = path + "/" + data["image"]
                    image = pygame.image.load(image_path)
                except:
                    raise ValueError(
                        "For create element, 'image' need to be valid path for valid image in " + name + " element")
                if data["type"] == "Image":
                    return image
                elif data["type"] == "Frame":
                    if "margin" in data:
                        if internal.correct_tuple(data["margin"], int, 4):
                            return frame.Frame(image, frame.Margin.from_tuple(data["margin"]))
                        else:
                            raise ValueError(
                                "For create Frame element, 'margin' need to be (int top, int left, int bottom, int right) in " + name + " element")
                    else:
                        raise ValueError(
                            "For create Frame element, it need to have 'margin' as property in " + name + " element")
                elif data["type"] == "Animation":
                    if "canvas" in data:
                        if internal.correct_tuple(data["canvas"], int, 2):
                            # Actually not implemented
                            ret = pygame.Surface(data["canvas"], pygame.HWSURFACE | pygame.SRCALPHA)
                            ret.blit(image, (0, 0))
                            return ret
                        else:
                            raise ValueError(
                                "For create Animation element, 'canvas' need to be (int width, int height) in " + name + " element")
                    else:
                        raise ValueError(
                            "For create Animation element, it need to have 'canvas' as property in " + name + " element")
            else:
                raise ValueError("For create element, it need to have 'image' as property in " + name + " element")
        else:
            raise ValueError("For create element, it need to have 'type' as property in " + name + " element")

    @staticmethod
    def getImage(cat, name):
        if Resources._loaded:
            if cat == Resources.MISC:
                return Resources.misc_images[name]
            elif cat == Resources.UI:
                return Resources.ui_images[name]
            elif cat == Resources.ICON:
                return Resources.icon_images[name]
            else:
                raise ValueError("cat need to be between 0 and 2")
        else:
            raise ValueError("Resources is not initialized")

    @staticmethod
    def getFont(name) -> text.Text:
        if name in range(1):
            return Resources.fonts[name]
        else:
            raise ValueError("name need to be between 0 and 1")

    @staticmethod
    def getColor(name):
        return Resources.colors[name]

    @staticmethod
    def getMetric(name):
        return Resources.metric[name]

    @staticmethod
    def readJSON(file):
        lines = open(file, 'r').read().split('\n')
        line = ""
        for i in lines:
            line += i
        return json.loads(line)

    @staticmethod
    def readFiles(path):
        image_descriptor_path = path + "/desc.json"
        image_descriptor = Resources.readJSON(image_descriptor_path)

        font_descriptor_path = path + "/font.json"
        font_descriptor = Resources.readJSON(font_descriptor_path)

        return image_descriptor, font_descriptor
